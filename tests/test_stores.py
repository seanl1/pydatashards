import pytest
import sys, os, tempfile
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from datashards.stores.memorystore import MemoryStore
from datashards.stores.simplefilestore import SimpleFileStore

def hello_world(bs=b'Hello World!'):
    # Return a null padded byte string
    pad_length = 32768 - len(bs) 
    return bs + b'\0' * pad_length


def test_memorystore():
    store = MemoryStore()
    # Test expected behavior
    data = hello_world()
    # We know the sha256 of this test shard
    expected_digest = 'dtpaEWX9InTZdJE6GRzVofjFmgQO7EBIvnSXjiS5JoU='
    expected_xt = f'urn:sha256:{expected_digest}' 
    xt = store.put(data)
    assert xt == expected_xt
    retrieved_data = store.get(xt)
    assert retrieved_data == hello_world()
    # Test race conditions
    with pytest.raises(ValueError):
        store.put(b'1' * 33)
    with pytest.raises(ValueError):
        store.put(b'a' * 31)
    with pytest.raises(KeyError):
        store.get('urn:sha256:abcdefghijlkmnop')
    with pytest.raises(ValueError):
        store.get('1234567890')
    with pytest.raises(ValueError):
        store.get('urn:sha1:abcdefghij')
    with pytest.raises(ValueError):
        store.get('a:sha256:abcdefghij')
    assert len(store.catalog()) == 1
    with pytest.raises(KeyError):
        store.delete('urn:sha256:abcdef')
    store.delete(xt)
    assert len(store.catalog()) == 0

def test_filestore():
    with tempfile.TemporaryDirectory() as dir:
        store = SimpleFileStore(dir)
        data = hello_world()
        expected_digest = 'dtpaEWX9InTZdJE6GRzVofjFmgQO7EBIvnSXjiS5JoU='
        xt = store.put(data)
        # Set the sha256 of 'Hello World'
        assert xt == f'urn:sha256:{expected_digest}'
        retrieved_data = store.get(xt)
        assert retrieved_data == data
        assert store.get('urn:sha256:abcdefghijlkmnop') == None
        assert len(store.catalog()) == 1
        with pytest.raises(ValueError):
            store.get('1234567890')
        with pytest.raises(ValueError):
            store.get('urn:sha1:abcdefghij')
        with pytest.raises(ValueError):
            store.get('a:sha256:abcdefghij')
    with pytest.raises(ValueError):
        store2 = SimpleFileStore('foo')
    # Make a temporary directory manually
    tempdir = os.path.join(tempfile.gettempdir(), 'testdatashards')
    store3 = SimpleFileStore(tempdir, create_dir=True)
    assert os.path.isdir(tempdir)
    os.rmdir(tempdir)

