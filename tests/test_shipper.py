import pytest
import sys, os, tempfile, base64, hashlib
import usexp
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from datashards.stores.memorystore import MemoryStore
import datashards.shipper

# Some constants to make things easier

SMALL_FILENAME = 'tests/gnu-head.png'
LARGE_FILENAME = 'tests/home_goblin.png'

SMALL_STATIC_URN = "magnet:?xt=urn:sha256:rfd84almYnmBYTZTKKnVL-p1GF5s33B9QWRjfrSJsP0=&ek=YWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE=&es=aes-ctr-sha256"
SMALL_STATIC_XT = "urn:sha256:rfd84almYnmBYTZTKKnVL-p1GF5s33B9QWRjfrSJsP0="

LARGE_STATIC_URN = "magnet:?xt=urn:sha256:R97f8N0fzSXgg5yhTJ4UsbEro4nk8lYn6pR1WR-A0_M=&ek=YWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE=&es=aes-ctr-sha256"
LARGE_STATIC_XT = "urn:sha256:R97f8N0fzSXgg5yhTJ4UsbEro4nk8lYn6pR1WR-A0_M="

def testkey():
    return b'a' * 32

@pytest.fixture(scope="module")
def shipper():
    store = MemoryStore()
    shipper = datashards.shipper.Shipper(store) 
    return shipper

def test_shipper_ship_small(shipper):
    fd = open(SMALL_FILENAME, 'rb')
    res = shipper.ship(fd, keyfun=testkey)
    fd.close()
    assert res == SMALL_STATIC_URN

def test_store_has_data(shipper):
    res = shipper.store.get(SMALL_STATIC_XT)
    assert res != None

def test_make_manifest():
    xts = [b'abc', b'def', b'ghi']
    # 31000 is less than 32k
    size = 31000
    res = datashards.shipper.make_manifest(xts, size)
    l = usexp.loadb(res)
    assert isinstance(l, list)
    assert l[0] == b'manifest'
    assert l[1] == b'31000'
    assert l[2] == xts[0]

def test_shipper_receive_small(shipper):
    output = tempfile.TemporaryFile()
    shipper.receive(SMALL_STATIC_URN, output)


def test_shipper_ship_large(shipper):
    fd = open(LARGE_FILENAME, 'rb')
    res = shipper.ship(fd, keyfun=testkey)
    fd.close()
    assert res == LARGE_STATIC_URN

def test_shipper_read_manifest(shipper):
    key = testkey()
    encrypted_manifest = shipper.store.get(LARGE_STATIC_XT)
    raw_manifest = datashards.shipper.decrypt_shard_entry(encrypted_manifest, key)
    manifest = usexp.loadb(raw_manifest)
    assert len(manifest) == 5

def test_shipper_receive_large(shipper):
    output = tempfile.TemporaryFile()
    shipper.receive(LARGE_STATIC_URN, output)

def test_shipper_small_full(shipper):
    SMALL_FILE_PATH = 'tests/gnu-head.png'
    fd = open(SMALL_FILENAME, 'rb')
    # This URN is unpredictable since the key is random
    urn = shipper.ship(fd)
    fd.close()
    output = tempfile.TemporaryFile()
    shipper.receive(urn, output)

def test_oversized_manifest():
    # This is completely bogus
    xts = [SMALL_STATIC_URN] * 500
    with pytest.raises(NotImplementedError):
        datashards.shipper.make_manifest(xts, 100000)
