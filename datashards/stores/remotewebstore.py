import requests

class RemoteWebStore():
    def __init__(self, base_url):
        # In the current DataShards, the base URL is everything, but that is likely to change
        self.base_url = base_url

    def get(self, xt):
        r = requests.get(self.base_url, params={'xt': xt})
        if not r.status_code == 200:
            return None
        return r.content

    def put(self, data):
        r = requests.post(self.base_url, data=data)
        # We should catch these errors
        r.raise_for_status()
        return r.content
