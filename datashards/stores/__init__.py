from .memorystore import MemoryStore
from .simplefilestore import SimpleFileStore
from .remotewebstore import RemoteWebStore