from setuptools import setup, find_packages

from datashards import name, version

setup(name=name,
      version=version,
      description='DataShards',
      url='',
      author='Serge Wroclawski',
      author_email='serge@wroclawski.org',
      license='Apache License 2.0',
      packages=find_packages(),
      classifiers=[
          'Intended Audience :: Developers',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3',
          'Topic :: Software Development :: Libraries',
      ]
)

