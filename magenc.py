import http.server
import socketserver

from datashards.stores.memorystore import MemoryStore
from datashards.stores.magencstore import MagencStore as handler
def run(port=9000):
    with socketserver.TCPServer(("", port), handler) as httpd:
        print("serving at port", port)
        httpd.serve_forever()

if __name__ == '__main__':
    run()

